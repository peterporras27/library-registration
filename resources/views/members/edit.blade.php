@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Add member</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<form action="{{ route('members.update', $member->id) }}" method="POST" enctype="multipart/form-data" role="form">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PUT">
			<div class="row">
				
				<div class="col-md-6">
					

					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">

							<label for="birthday">Birth Day</label>
							<div class="input-group date" style="direction: rtl;">
		                        <input type="text" value="{{ $member->birthdate }}" placeholder="{{ date('m/d/Y') }}" name="birthdate" id="birthdate" class="form-control">
		                        <span class="input-group-addon">
		                        	<i class="fa fa-calendar"></i>
		                        </span>
		                    </div>
		                    <br>

							<div class="form-group">
								<label for="address">Address</label>
								<input type="text" value="{{ $member->address }}" name="address" class="form-control" id="address" placeholder="Address">
							</div>

							<div class="form-group">
								<label for="phone_number">Phone Number</label>
								<input type="text" value="{{ $member->phone_number }}" name="phone_number" class="form-control" id="phone_number" placeholder="Phone Number">
							</div>

							<label for="renewal_date">Valid Until</label>
							<div class="input-group date" style="direction: rtl;">
		                        <input type="text" value="{{ $member->renewal_date }}" placeholder="{{ date('m/d/Y') }}" name="renewal_date" id="renewal_date" class="form-control">
		                        <span class="input-group-addon">
		                        	<i class="fa fa-calendar"></i>
		                        </span>
		                    </div>
		                    <br>
		                    <button type="submit" class="btn btn-primary btn-lg">Save member <i class="fa fa-save"></i></button>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">

							@if( $photo )
								<img src="{{ asset($photo) }}" class="img-responsive" alt="">
							@endif

							<div class="form-group">
								<label for="photo">Photo</label>
								<input type="file" value="" name="photo" class="form-control" id="photo" placeholder="Address">
							</div>	
							
						</div>
					</div>

				</div>

				<div class="col-md-6">

					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" value="{{ $member->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="First name">
					</div>

					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" value="{{ $member->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="Last name">
					</div>

					<div class="form-group">
						<label for="middle_name">Middle Name</label>
						<input type="text" value="{{ $member->middle_name }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle name">
					</div>

					<div class="form-group">
						<label for="gender">Gender</label>
						<select name="gender" class="form-control" id="gender">
							<option value="male"{{ $member->gender=='male' ? ' selected':'' }}>Male</option>
							<option value="female"{{ $member->gender=='female' ? ' selected':'' }}>Female</option>
						</select>
					</div>

					<div class="form-group">
						<label for="name">Activate Member</label><br>
						<input type="checkbox" name="active" value="1" class="js-switch" {{ $member->active ? 'checked':'' }} />
						<span id="active-status">Active</span>
					</div>

				</div>
			</div>
		
		</form>
		
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('members.index') }}" class="btn btn-white">
	Back <i class="fa fa-chevron-left"></i> 
</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}">
<link href="{{asset('css/plugins/switchery/switchery.css')}}" rel="stylesheet">
<style>.datepicker{right:auto;}</style>
@endsection

@section('scripts')
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/plugins/switchery/switchery.js')}}"></script>
<script>
	jQuery(document).ready(function($) {
		$('#birthdate, #renewal_date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
        var switchBtn = document.querySelector('.js-switch');
        var switchButton = new Switchery(switchBtn, { color: '#1AB394' });
        switchBtn.onchange = function() {
        	if ( switchBtn.checked ) {
        		$('.js-switch').attr('checked','checked');
        		$('#active-status').text('Active');
        	} else {
        		$('.js-switch').removeAttr('checked');
        		$('#active-status').text('Inactive');
        	}
		};
	});
</script>
@endsection