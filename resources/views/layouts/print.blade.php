<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Print Report</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<style>table{font-size: 11px;}</style>
	</head>
	<body>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<br>

					<h3 align="center">Log Report</h3>
					<h5 align="center">Tomas Confesor Memorial Public Library Monitoring System</h5>
					@if( !empty($start) && !empty($end) )
					<p align="center">From <strong>{{ $start }}</strong> to <strong>{{ $end }}</strong></p>
					@endif
					<hr>

					@if( $logs->count() )
			
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th><strong>First Name</strong></th>
									<th><strong>Last Name</strong></th>
									<th><strong>Address</strong></th>
									<th><strong>Purpose</strong></th>
									<th><strong>Log Type</strong></th>
									<th><strong>Time</strong></th>
								</tr>
							</thead>
							<tbody>
								@foreach( $logs as $log )

									<tr id="user-row-{{ $log->id }}">
										<td>{{ $log->member()->first_name }}</td>
										<td>{{ $log->member()->last_name }}</td>
										<td>{{ $log->member()->address }}</td>
										<td>{{ $log->purpose }}</td>
										<td>{{ $log->log_type }}</td>
										<td>{{ date( 'M d, Y h:i a', strtotime( $log->created_at ) ) }}</td>
									</tr>

								@endforeach
							</tbody>
						</table>
					</div>
					{{ $logs->appends( request()->input() )->links() }}
					@else
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>No records to show at the moment.</strong>
					</div>
					@endif


				</div>
			</div>
		</div>
		<script>window.print();</script>
	</body>
</html>