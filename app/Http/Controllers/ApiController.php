<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\MemberLog;
use QrCode;

class ApiController extends Controller
{
    // https://www.simplesoftware.io/docs/simple-qrcode#docs-usage
    public function qr( $size = 100, $format = 'png', $key )
    {
    	$size = preg_replace('/\D/', '', $size );
    	$size = ( empty( $size ) ) ? 100: $size;
    	$formats = array('png','eps','svg');
    	$format = ( in_array($format, $formats) ) ? $format:'png';
        return QrCode::format( $format )->size( $size )->generate( $key );
    }

    public function library_card( $id ){

        $member = Member::find( $id );

        if ( !$member ) {
            return redirect('/');
        }

        return view( 'layouts.id', ['member' => $member ] );
    }

    public function api_log( Request $request )
    {

        $response = array(
            'error' => false,
            'message' => '',
            'data' => '',
        );

        // Check if event key exist
        if ( !$request->input('card_number') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: member not found.';
            return response()->json( $response );
        }

        // Find user who owns specific hash code.
        $member = Member::where( 'card_number', '=', $request->input('card_number') )->first();

        // Check if member exist.
        if ( ! $member ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: member not found.';
            return response()->json( $response );
        }

        // Check if login type
        if ( !$request->input('type') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        $logTypes = array('login','logout');
        if ( !in_array( $request->input('type'), $logTypes ) ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        $logtype = strtolower( $request->input('type') );

        $name = $member->first_name . ' ' . $member->last_name;

        $log = new MemberLog();
        $log->log_type = $logtype; // login, logout, onetime
        $log->purpose = 'Research';
        $log->member_id = $member->id;
        $log->save();

        $response['message'] = 'SUCCESS: '.$name . ' log recorded!';
        

        return response()->json( $response );
    }
}
