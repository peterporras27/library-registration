<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'gender',
        'birthdate',
        'address',
        'phone_number',
        'card_number',
        'active',
        'renewal_date',
    ];

    public function logs()
    {
      
        return $this->hasMany(
            'App\MemberLog', 
            'member_id', // foreign key
            'id' // local key
        )->first();
    }
}
